from os import environ
from os.path import join as path_join

home = path_join(environ.get("HOME"), ".config", "qtile")
mod = "mod1"  # Alt
mod_windows = "mod4"  # Win
terminal = "kitty"
