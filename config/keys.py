from libqtile.lazy import lazy
from libqtile.config import Key
from .envs import mod, mod_windows, home, terminal

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    Key(
        [mod_windows],
        "space",
        lazy.layout.next(),
        desc="Move window focus to other window (with mod_windows key)",
    ),
    # Move windows between left/right columns or move up/down in current stack.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Launch apps
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "b", lazy.spawn("opera"), desc="Launch browser"),
    Key([mod], "m", lazy.spawn("spotify"), desc="Launch music player"),
    Key([mod], "e", lazy.spawn("thunderbird"), desc="Launch E-mail manager"),
    Key([mod], "f", lazy.spawn(f"{terminal} ranger"), desc="Launch file browser"),
    # Toggle between different layouts and window controls
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawn("rofi -show drun"), desc="Spawn a command using a prompt widget"),
    Key([mod, "shift"], "r", lazy.spawn("rofi -show window"), desc="Spawn a window change"),
    # Mouse control
    Key([], "XF86TouchpadToggle", lazy.spawn(f"{home}/scripts/mouse.sh"), desc="Toggle touchpad"),
    # Volume control
    Key(
        [],
        "XF86AudioLowerVolume",
        lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%"),
        desc="Volume raise up",
    ),
    Key(
        [],
        "XF86AudioRaiseVolume",
        lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%"),
        desc="Volume raise down",
    ),
    Key(
        [],
        "XF86AudioMute",
        lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle"),
        desc="Volume mute control",
    ),
    # Play track control
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous"), desc="Play previous song"),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next"), desc="Play next song"),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause"), desc="Toggle song"),
    Key([], "XF86AudioStop", lazy.spawn("playerctl stop"), desc="Stop song"),
    # Brightness control
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +10%"), desc="Brightness up"),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 10%-"), desc="Brightness down"),
    Key([], "XF86ScreenSaver", lazy.spawn(f"{home}/scripts/redshift.sh"), desc="Toggle night light"),
]
