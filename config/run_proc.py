from subprocess import Popen as Command
from typing import List


def run_list(commands: List[List[str]]) -> List[Command]:
    """Create a list of running process from the list"""
    cmds = list()
    for cmd in commands:
        cmds.append(Command(cmd))

    return cmds


def wait_finish(commands: List[Command]):
    """
    Await for all commands until finish
    NOTE: One by one in order
    """
    for cmd in commands:
        cmd.wait()
