from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Match, Screen
from socket import gethostname

from .run_proc import run_list, wait_finish
from .keys import keys, lazy, Key
from .envs import mod_windows, mod, home, path_join

init_commands = run_list(
    [
        (path_join(home, "scripts", "pulseaudio.sh"),),
        (path_join(home, "scripts", "picom.sh"),),
        (path_join(home, "scripts", "background.sh"),),
        ("brightnessctl", "s", "250"),
        ("setxkbmap", "latam"),
    ]
)

groups = [Group(name=i) for i in "asdfop"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod_windows], i.name, lazy.group[i.name].toscreen(), desc="Switch to group {}".format(i.name)
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod_windows, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + control + letter of group = move focused window to group
            Key(
                [mod_windows, "control"],
                i.name,
                lazy.window.togroup(i.name),
                desc="move focused window to group {}".format(i.name),
            ),
        ]
    )

layouts = [
    layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    layout.Max(),
    layout.MonadTall(),
]

widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.TextBox(gethostname(), name="default"),
                widget.Systray(),
                widget.Clock(format="%Y-%m-%d %H:%M:%S"),
            ],
            20,
        ),
    ),
]

# Drag floating layouts.
# Button1 -> Left click
# Button2 -> Middle click
# Button3 -> Right click
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = True

# JAVA support
wmname = "LG3D"

wait_finish(init_commands)
