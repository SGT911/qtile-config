#!/bin/bash

FILE_CHECK="/tmp/$USER.pulseaudio"

if [ ! -f "$FILE_CHECK" ]; then
	pulseaudio -D
	touch "$FILE_CHECK"
else
	pulseaudio -k
	rm "$FILE_CHECK"
	$0
fi
