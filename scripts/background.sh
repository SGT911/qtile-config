#!/bin/bash

FILE_CHECK="/tmp/$USER.background"
VIDEO_FILE="$HOME/.config/qtile/data/wallpaper.mp4"

if [ ! -f "$FILE_CHECK" ]; then
	xwinwrap -ov -fs -- mplayer -wid WID -loop 0 -ao null -vo x11 -noconsolecontrols $VIDEO_FILE &
	touch "$FILE_CHECK"
fi
