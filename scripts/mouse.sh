#!/bin/bash

FILE_CHECK="/tmp/$USER.mouse_enabled"

if [ ! -f "$FILE_CHECK" ]; then
	# In this case the device touchpad is (14)
	xinput disable 14
	touch "$FILE_CHECK"
else
	xinput enable 14
	rm "$FILE_CHECK"
fi