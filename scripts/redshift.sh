#!/bin/bash

FILE_CHECK="/tmp/$USER.redshift"
REDCOLOR=3500

if [ -f "$FILE_CHECK" ]; then
	redshift -x
	rm "$FILE_CHECK"
else
	redshift -O 3500
	touch "$FILE_CHECK"
fi