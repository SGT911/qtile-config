#!/bin/bash

FILE_CHECK="/tmp/$USER.picom"

if [ ! -f "$FILE_CHECK" ]; then
	picom --vsync &
	touch "$FILE_CHECK"
fi
